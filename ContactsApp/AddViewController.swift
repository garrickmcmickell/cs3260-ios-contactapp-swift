//
//  AddViewController.swift
//  ContactsApp
//
//  Created by Garrick McMickell on 11/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import CoreData

class AddViewController: UIViewController {

    @IBOutlet var address: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var firstName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func saveBtnTouched(_ sender: UIBarButtonItem) {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Contact", in: context)
        let contact = NSManagedObject(entity: entity!, insertInto: context)
        
        if (firstName.text == "") {
            let alert = UIAlertController(title: "", message: "You must at least enter a first name", preferredStyle: .alert)
            let action = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            contact.setValue(firstName.text, forKey: "firstName")
            contact.setValue(lastName.text, forKey: "lastName")
            contact.setValue(phone.text, forKey: "phone")
            contact.setValue(email.text, forKey: "email")
            contact.setValue(address.text, forKey: "address")
        
            do {
                try context.save()
                navigationController?.popToRootViewController(animated: true)
            }
            catch {
            
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
