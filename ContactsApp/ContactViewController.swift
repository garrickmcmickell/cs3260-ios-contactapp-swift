//
//  ContactViewController.swift
//  ContactsApp
//
//  Created by Garrick McMickell on 11/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import MapKit

class ContactViewController: UIViewController {

    @IBOutlet var firstName: UILabel!
    @IBOutlet var lastName: UILabel!
    @IBOutlet var phone: UIButton!
    @IBOutlet var email: UIButton!
    @IBOutlet var address: UILabel!
    @IBOutlet var map: MKMapView!
    
    var contact = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        firstName.text = (contact[0] as AnyObject).value(forKey: "firstName") as! String
        lastName.text = (contact[0] as AnyObject).value(forKey: "lastName") as! String
        phone.setTitle((contact[0] as AnyObject).value(forKey: "phone") as! String, for: .normal)
        email.setTitle((contact[0] as AnyObject).value(forKey: "email") as! String, for: .normal)
        address.text = (contact[0] as AnyObject).value(forKey: "address") as! String
    
        let addr = (contact[0] as AnyObject).value(forKey: "address") as! String
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(addr, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error)
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                self.centerMapOnLocation(location: coordinates)

                var pin = MKPointAnnotation()
                pin.coordinate = coordinates
                self.map.addAnnotation(pin)
            }
        })
    
    }

    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let regionRadius: CLLocationDistance = 1000
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location,                                                                  regionRadius * 2.0, regionRadius * 2.0)
        map.setRegion(coordinateRegion, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editBtnTouched(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "moveToEdit", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "moveToEdit") {
            let editVC = segue.destination as! EditViewController
            editVC.contact = contact
        }
    }
    
    @IBAction func callBtnTouched(_ sender: UIButton) {
        let phone = (contact[0] as AnyObject).value(forKey: "phone") as! String
        
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phone)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL)) {
                application.openURL(phoneCallURL as URL);
            }
        }
    }

    @IBAction func emailBtnTouched(_ sender: AnyObject) {
        let email = (contact[0] as AnyObject).value(forKey: "email") as! String
        
        if let mailURL:NSURL = NSURL(string: "mailto://\(email)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(mailURL as URL)) {
                application.openURL(mailURL as URL);
            }
        }
    }

}




