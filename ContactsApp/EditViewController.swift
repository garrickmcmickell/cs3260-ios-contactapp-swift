//
//  EditViewController.swift
//  ContactsApp
//
//  Created by Garrick McMickell on 11/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import CoreData

class EditViewController: UIViewController {
    
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var address: UITextField!
    
    var contact = [Any]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        firstName.text = (contact[0] as AnyObject).value(forKey: "firstName") as! String
        lastName.text = (contact[0] as AnyObject).value(forKey: "lastName") as! String
        phone.text = (contact[0] as AnyObject).value(forKey: "phone") as! String
        email.text = (contact[0] as AnyObject).value(forKey: "email") as! String
        address.text = (contact[0] as AnyObject).value(forKey: "address") as! String
    }

    @IBAction func saveBtnTouched(_ sender: UIBarButtonItem) {
        let editContact = contact[0] as! NSManagedObject
        
        if (firstName.text == "") {
            let alert = UIAlertController(title: "", message: "You must at least enter a first name", preferredStyle: .alert)
            let action = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            editContact.setValue(firstName.text, forKey: "firstName")
            editContact.setValue(lastName.text, forKey: "lastName")
            editContact.setValue(phone.text, forKey: "phone")
            editContact.setValue(email.text, forKey: "email")
            editContact.setValue(address.text, forKey: "address")
        
            do {
                try editContact.managedObjectContext?.save()
                navigationController?.popToRootViewController(animated: true)
            }
            catch {
            
            }
        }
    }
    
    @IBAction func deleteBtnClicked(_ sender: UIButton) {
        let editContact = contact[0] as! NSManagedObject
        
        editContact.managedObjectContext?.delete(editContact)
        
        do {
            try editContact.managedObjectContext?.save()
            navigationController?.popToRootViewController(animated: true)
        }
        catch {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
