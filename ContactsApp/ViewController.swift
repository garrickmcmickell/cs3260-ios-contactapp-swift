//
//  ViewController.swift
//  ContactsApp
//
//  Created by Garrick McMickell on 11/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var array = ([Any])()

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
        tableView.reloadData()
    }
    
    func getData() -> Void {
        let context = getContext()
        
        
        let fetch: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Contact")
        
        do{
            let result = try context.fetch(fetch)
            array = result
        }
        catch {
            
        }
    }

    @IBAction func addBtnTouched(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "moveToAdd", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "moveToContact", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "moveToContact") {
            let contactVC = segue.destination as! ContactViewController
            let ip = sender as! NSIndexPath
            contactVC.contact = [array[ip.row]]
        }
        if (segue.identifier == "moveToAdd") {
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let object = array[indexPath.row]
        
        cell.textLabel?.text = (object as AnyObject).value(forKey: "firstName") as? String
        
        return cell
    }
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

